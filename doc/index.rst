##################
Currency RO Module
##################

The *Currency RO Module* adds the `Romanian National Bank <https://bnr.ro/>` as
a source for currency exchange rates.

.. toctree::
   :maxdepth: 2

   configuration
   design
